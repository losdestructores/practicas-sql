## Ejercicios propuestos
# Una empresa almacena la información de sus clientes en una tabla llamada "clientes".
# 01 Elimine la tabla:
'''sql
drop table clientes;
'''
# 02 Elimine la vista "vista_clientes":
'''sql
drop view vista_clientes;
'''
# 03 Intente crear o reemplazar la vista "vista_clientes" para que muestre el nombre, domicilio y ciudad de todos los clientes de "Cordoba" (sin emplear "force")
Mensaje de error porque la tabla referenciada no existe.
'''sql
'''
# 04 Cree o reemplace la vista "vista_clientes" para que recupere el nombre, apellido y ciudad de todos los clientes de "Cordoba" empleando "force"
'''sql
'''
# 05 Cree la tabla:
'''sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);
'''
# 06 Ingrese algunos registros:
'''sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');

'''
# 07 Cree o reemplace la vista "vista_clientes" para que muestre todos los campos de la tabla "clientes" 
'''sql
'''
# 08 Consulte la vista
'''sql
'''
# 09 Agregue un campo a la tabla "clientes"
'''sql
'''
# 10 Consulte la vista "vista_clientes" El nuevo campo agregado a "clientes" no aparece, pese a que la vista indica que muestre todos los campos de dicha tabla.
'''sql
'''
# 11 Modifique la vista para que aparezcan todos los campos.
'''sql
'''
