## Ejercicios propuestos
# Una empresa almacena la información de sus clientes en una tabla llamada "clientes".
# 01 Elimine la tabla:
'''sql
drop table clientes;
'''
# 02 Cree la tabla:
'''sql
create table clientes(
    nombre varchar2(40),
    documento char(8),
    domicilio varchar2(30),
    ciudad varchar2(30)
);
'''
# 03 Ingrese algunos registros:
'''sql
insert into clientes values('Juan Perez','22222222','Colon 1123','Cordoba');
insert into clientes values('Karina Lopez','23333333','San Martin 254','Cordoba');
insert into clientes values('Luis Garcia','24444444','Caseros 345','Cordoba');
insert into clientes values('Marcos Gonzalez','25555555','Sucre 458','Santa Fe');
insert into clientes values('Nora Torres','26666666','Bulnes 567','Santa Fe');
insert into clientes values('Oscar Luque','27777777','San Martin 786','Santa Fe');
insert into clientes values('Pedro Perez','28888888','Colon 234','Buenos Aires');
insert into clientes values('Rosa Rodriguez','29999999','Avellaneda 23','Buenos Aires');

'''
# 04 Cree o reemplace la vista "vista_clientes" para que recupere el nombre y ciudad de todos los clientes que no sean de "Cordoba" sin emplear "with check option"
'''sql

'''
# 05 Cree o reemplace la vista "vista_clientes2" para que recupere el nombre y ciudad de todos los clientes que no sean de "Cordoba" empleando "with check option" 
'''sql
'''
# 06 Consulte ambas vistas
'''sql

'''
# 07 Intente modificar la ciudad del cliente "Pedro Perez" a "Cordoba" través de la vista que está restringida.
'''sql

'''
# 08 Realice la misma modificación que intentó en el punto anterior a través de la vista que no está restringida
'''sql
'''
# 09 Actualice la ciudad del cliente "Oscar Luque" a "Buenos Aires" mediante la vista restringida
'''sql
'''
# 10 Verifique que "Oscar Luque" aún se incluye en la vista
'''sql
'''
# 11 Intente ingresar un empleado de "Cordoba" en la vista restringida
'''sql
'''
# 12 Ingrese el empleado anterior a través de la vista no restringida
'''sql
'''
# 13 Ingrese un empleado de "Salta" en la vista restringida
'''sql
'''
# 14 Verifique que el nuevo registro está incluido en la vista
'''sql
'''