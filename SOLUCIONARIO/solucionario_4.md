# solucionario - Crear tablas (create table - describe - all_tables - drop table)

## practicas de laboratorio 

# EJERCICIO 01
## Intente crear una tabla llamada "*agenda"
codigo sql:


```sql

create table*agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);


```
salida del codigo

```sh
lInforme de error -
ORA-00903: nombre de tabla no válido
00903. 00000 -  "invalid table name"
*Cause:    
*Action:
```
APARECE UN MENSAJE DE ERROR INDICANDO QUE USAMOS UN CARACTER INAVALIDO (*) PARA EL NOBRE DE LA TABLA.

# SOLUCION 01

```sql
create table agenda(
    apellido varchar2(30),
    nombre varchar2(20),
    domicilio varchar2(30),
    telefono varchar2(11)
);

```

```sql

HR> create table agenda(
        apellido varchar2(30),
        nombre varchar2(20),
        domicilio varchar2(30),
        telefono varchar2(11)
    )
[2023-06-01 14:01:49] completed in 294 ms


```
# EJERCICIO 02
## Necesita almacenar información referente a los libros de su biblioteca personal. Los datos que guardará serán: título del libro, nombre del autor y nombre de la editorial.


#### 1 Elimine la tabla "libros" Si no existe, un mensaje indica tal situación.


#### 2 Verifique que la tabla "libros" no existe (all_tables) No aparece en la lista.


#### 3 Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo, varchar2(20); autor, varchar2(30) y editorial, varchar2(15)


#### 4 Intente crearla nuevamente: Aparece mensaje de error indicando que existe un objeto con el nombre "libros".


#### 5 Visualice las tablas existentes


#### 6 Visualice la estructura de la tabla "libros": Aparece "libros" en la lista.


#### 7 Elimine la tabla


#### 8 Intente eliminar la tabla Un mensaje indica que no existe.

CODIGO SQL :
# EJERCICIO 02

```sql
SELECT table_name FROM all_tables WHERE table_name = 'libros';
CREATE TABLE libros (
  titulo VARCHAR2(20),
  autor VARCHAR2(30),
  editorial VARCHAR2(15)
);
SELECT table_name FROM all_tables;
DESCRIVE libros;
DROP TABLE libros;

```
SALIDA DEL CODIGO

```sql

TABLE_NAME

```
