## Practica de laboratorio
# Una empresa almacena los datos de sus empleados en una tabla llamada "empleados".
# 01 Eliminamos la tabla y la creamos:
'''sql
drop table empleados;

create table empleados(
    documento char(8),
    nombre varchar2(20),
    apellido varchar2(20),
    sueldo number(6,2),
    cantidadhijos number(2,0),
    fechaingreso date,
    primary key(documento)
);

'''
# 02 Ingrese algunos registros:
'''sql
insert into empleados values('22222222','Juan','Perez',200,2,'10/10/1980');
insert into empleados values('22333333','Luis','Lopez',250,0,'01/02/1990');
insert into empleados values('22444444','Marta','Perez',350,1,'02/05/1995');
insert into empleados values('22555555','Susana','Garcia',400,2,'15/12/2018');
insert into empleados values('22666666','Jose Maria','Morales',500,3,'25/08/2015');

'''
# 03 Cree (o reemplace) el procedimiento almacenado llamado "pa_aumentarsueldo" que aumente los sueldos inferiores al promedio en un 20% 
'''sql

'''
# 04 Ejecute el procedimiento creado anteriormente
'''sql

'''
# 05 Verifique que los sueldos han aumentado
'''sql
'''
# 06 Ejecute el procedimiento nuevamente
'''sql
'''
# 07 Verifique que los sueldos han aumentado
'''sql
'''
# 08 Elimine la tabla "empleados_antiguos"
'''sql
'''
# 09 Cree la tabla "empleados_antiguos"
'''sql
create table empleados_antiguos(
    documento char(8),
    nombre varchar2(40)
);
'''
# 10 Cree (o reemplace) un procedimiento almacenado que ingrese en la tabla "empleados_antiguos" el documento, nombre y apellido (concatenados) de todos los empleados de la tabla "empleados" que ingresaron a la empresa hace más de 10 años
'''sql
'''
# 11 Ejecute el procedimiento creado anteriormente
'''sql
'''
# 12 Verifique que la tabla "empleados_antiguos" ahora tiene registros (3 registros)
'''sql
'''