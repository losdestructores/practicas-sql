# Ingresar registros (insert into- select)
## Ejercicio 01
#### Trabaje con la tabla "agenda" que almacena información de sus amigos.


#### Elimine la tabla "agenda"
CODIGO SQL

```sql

DROP TABLE agenda;

```
SALIDA

```sql

Table AGENDA borrado.

```


#### Cree una tabla llamada "agenda". Debe tener los siguientes campos: apellido (cadena de 30), nombre (cadena de 20), domicilio (cadena de 30) y telefono (cadena de 11)

CODIGO SQL

```sql

CREATE TABLE agenda (
  apellido VARCHAR2(30),
  nombre VARCHAR2(20),
  domicilio VARCHAR2(30),
  telefono VARCHAR2(11)
);

```
SALIDA 

```sql

Table AGENDA creado.

```


#### Visualice las tablas existentes para verificar la creación de "agenda" (all_tables)

CODIGO SQL
```sql
SELECT table_name FROM all_tables;

```
SALIDA

```sql

AGENDA

```
#### Visualice la estructura de la tabla "agenda" (describe)
```sql
DESCRIVE AGENDA

```
```sql
SALIDA

Table AGENDA creado.
Nombre    ¿Nulo?  Tipo
--------- ------  -----------
APELLIDO          VARCHAR2(30)
NOMBRE            VARCHAR2(20)
DOMICILIO         VARCHAR2(30)
TELEFONO          VARCHAR2(11)  

```

#### Ingrese los siguientes registros:
CODIGO SQL 
```sql

insert into agenda (apellido, nombre, domicilio, telefono) values ('Moreno','Alberto','Colon 123','4234567'); insert into agenda (apellido,nombre, domicilio, telefono) values ('Torres','Juan','Avellaneda 135','4458787');

```
SALIDA
```sql
1 fila insertadas.


1 fila insertadas

```

#### Seleccione todos los registros de la tabla.
CODIGO SQL
```sql

SELECT * FROM agenda;

```
SALIDA
```sql

MANU	Alberto	Colon 123	    4234567
Torres	Juan	Avellaneda 135	4458787

```
#### Elimine la tabla "agenda"
CODIGO SQL
```sql

DROP TABLE agenda;

```
SALIDA
```sql
Table AGENDA borrado;
```


#### Intente eliminar la tabla nuevamente (aparece un mensaje de error
CODIGO SQL
```sql

DROP TABLE agenda;

```
SALIDA

```sql
Error que empieza en la línea: 17 del comando :
DROP TABLE agenda
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:

```

## EJERCICIO 02

### -Trabaje con la tabla "libros" que almacena los datos de los libros de su propia biblioteca.
#### Elimine la tabla "libros"
CODIGO SQL

```sql

DROP TABLE libros;

```
SALIDA 
```sql

Error que empieza en la línea: 2 del comando :
DROP TABLE libros
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:
```
#### -Cree una tabla llamada "libros". Debe definirse con los siguientes campos: titulo (cadena de 20), autor (cadena de 30) y editorial (cadena de 15)
CODIGO SQL
```sql
CREATE TABLE libros (
  titulo VARCHAR(20),
  autor VARCHAR(30),
  editorial VARCHAR(15)
);

```
SALIDA

```sql

Table LIBROS creado.

```
#### -Visualice las tablas existentes.

codigo sql

```sql
SELECT table_name
FROM  all_tables;

```
salida
```sql

67 LIBROS

```
#### Visualice la estructura de la tabla "libros"

CODIGO SQL
```sql
DESCRIBE libros;

```
SALIDA 

```sql
Nombre    ¿Nulo? Tipo         
--------- ------ ------------ 
TITULO           VARCHAR2(20) 
AUTOR            VARCHAR2(30) 
EDITORIAL        VARCHAR2(15) 


```

#### Ingrese los siguientes registros:

CODIGO SQL

```sql
INSERT INTO libros (titulo, autor, editorial) VALUES ('El aleph', 'Borges', 'Planeta');
INSERT INTO libros (titulo, autor, editorial) VALUES ('Martin Fierro', 'Jose Hernandez', 'Emece');
INSERT INTO libros (titulo, autor, editorial) VALUES ('Aprenda PHP', 'Mario Molina', 'Emece');

```
```sql

1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```

#### Muestre todos los registros (select) de "libros"

CODIGO SQL
```sql

SELECT * FROM libros;


```
SALIDA 

```sql
1 El aleph	    Borges	        Planeta
2 Martin Fierro	Jose Hernandez	Emece
3 Aprenda PHP	    Mario Molina	Emece

```




