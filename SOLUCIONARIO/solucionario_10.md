# Ejercicio 02
## Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.

#### Elimine la tabla si existe.
```sql
DROP TABLE empleados;

```
#### Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);

```

#### Verifique que la tabla existe consultando
```sql

SELECT table_name
FROM all_tables
WHERE table_name = 'EMPLEADOS';

```

#### Vea la estructura de la tabla (5 campos)

```sql
DESCRIBE empleados;

```
#### Ingrese algunos registros: 

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);

```
#### Seleccione todos los registros (3 registros)

```sql
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico)
VALUES ('María González','29999888','masculino','Belgrano 567',700);


```
#### Intente ingresar un registro con el valor "masculino" en el campo "sexo".

```sql
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico)
VALUES ('María González','29999888','masculino','Belgrano 567',700);

```
#### Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico" Mensaje de error.

```sql

INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico)
VALUES ('Pedro Ramirez','28999777','m','San Martín 890',10000);

```
#### Elimine la tabla
```sql
DROP TABLE empleados;

```


