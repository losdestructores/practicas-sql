#  Tipos de datos 

## Ejercicio 01

#### Un videoclub que alquila películas en video almacena la información de sus películas en una tabla llamada "peliculas"; para cada película necesita los siguientes datos:

#### Elimine la tabla "peliculas"
CODIGO SQL

```sql

DROP TABLE peliculas;

```
SALIDA

```sql

Error que empieza en la línea: 2 del comando :
DROP TABLE  peliculas
Informe de error -
ORA-00942: la tabla o vista no existe
00942. 00000 -  "table or view does not exist"
*Cause:    
*Action:


```
#### Cree la tabla eligiendo el tipo de dato adecuado para cada campo.
CODIGO SQL
```sql
CREATE TABLE peliculas (
  nombre VARCHAR(20),
  actor VARCHAR(20),
  duracion NUMBER(3),
  cantidad NUMBER(1)
);

```
SALIDA 
```sql

Table PELICULAS creado.

```
#### Vea la estructura de la tabla.
CODIGO SQL

```sql
DESCRIVE pelicula;

```
SALIDA 
```sql
Nombre   ¿Nulo? Tipo         
-------- ------ ------------ 
NOMBRE          VARCHAR2(20) 
ACTOR           VARCHAR2(20) 
DURACION        NUMBER(3)    
CANTIDAD        NUMBER(1)    

```
#### Ingrese los siguientes registros:

CODIGO SQL
```sql

insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible','Tom Cruise',128,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mision imposible 2','Tom Cruise',130,2);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Julia Roberts',118,3);
insert into peliculas (nombre, actor, duracion, cantidad) values ('Elsa y Fred','China Zorrilla',110,2);


```
SALIDA 
```sql
1 fila insertadas.


1 fila insertadas.


1 fila insertadas.


1 fila insertadas.

```
#### Muestre todos los registros (4 registros)
CODIGO SQL 
```sql
SELECT * FROM peliculas;

```
SALIDA 
```sql

1Mision imposible	Tom Cruise	    128	3
2Mision imposible 2	Tom Cruise	    130	2
3Mujer bonita	    Julia Roberts	118	3
4Elsa y Fred	China   Zorrilla	    110	2

```

#### Intente ingresar una película con valor de cantidad fuera del rango permitido:
codigo sql
```sql
 insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10);

```
salida 
```sql

insert into peliculas (nombre, actor, duracion, cantidad)
  values ('Mujer bonita','Richard Gere',1200,10)
Error en la línea de comandos : 26 Columna : 41
Informe de error -
Error SQL: ORA-01438: valor mayor que el que permite la precisión especificada para esta columna
01438. 00000 -  "value larger than specified precision allowed for this column"
*Cause:    When inserting or updating records, a numeric value was entered
           that exceeded the precision defined for the column.
*Action:   Enter a value that complies with the numeric column's precision,
           or use the MODIFY option with the ALTER TABLE command to expand
           the precision.


```
#### Ingrese un valor con decimales en un nuevo registro, en el campo "duracion":

CODIGO SQL
```sql
insert into peliculas (nombre, actor, duracion, cantidad) values ('Mujer bonita','Richard Gere',120.20,4);

```
SALIDA
```sql
1 fila insertadas.

```
#### Muestre todos los registros para ver cómo se almacenó el último registro ingresado.
CODIGO SQL
```sql
Mision imposible	Tom Cruise	128	3

```
SALIDA 
```sql
Mision imposible	Tom Cruise	128	3
Mision imposible 2	Tom Cruise	130	2
Mujer bonita	Julia Roberts	118	3
Elsa y Fred	    China Zorrilla	110	2
Mujer bonita	Richard Gere	120	4

```
#### Intente ingresar un nombre de película que supere los 20 caracteres.
CODIGO SQL
```sql
INSERT INTO peliculas (nombre, actor, duracion, cantidad) VALUES ('LA PELICULA DE MUY MUY LEJANO DEL MUNDO ZEIN', 'Actor X', 90, 1);

```

SALIDA 
```sql
Informe de error -
Error SQL: ORA-12899: el valor es demasiado grande para la columna "BRADDOCK"."PELICULAS"."NOMBRE" (real: 44, máximo: 20)
12899. 00000 -  "value too large for column %s (actual: %s, maximum: %s)"
*Cause:    An attempt was made to insert or update a column with a value
           which is too wide for the width of the destination column.
           The name of the column is given, along with the actual width
           of the value, and the maximum allowed width of the column.
           Note that widths are reported in characters if character length
           semantics are in effect for the column, otherwise widths are
           reported in bytes.
*Action:   Examine the SQL statement for correctness.  Check source
           and destination column data types.
           Either make the destination column wider, or use a subset
           of the source column (i.e. use substring).

```
# Ejercicio 02
## Una empresa almacena los datos de sus empleados en una tabla "empleados" que guarda los siguientes datos: nombre, documento, sexo, domicilio, sueldobasico.

#### Elimine la tabla si existe.
```sql
DROP TABLE empleados;

```
#### Cree la tabla eligiendo el tipo de dato adecuado para cada campo:
```sql
create table empleados(
    nombre varchar2(20),
    documento varchar2(8),
    sexo varchar2(1),
    domicilio varchar2(30),
    sueldobasico number(6,2)
);

```

#### Verifique que la tabla existe consultando
```sql

SELECT table_name
FROM all_tables
WHERE table_name = 'EMPLEADOS';

```

#### Vea la estructura de la tabla (5 campos)

```sql
DESCRIBE empleados;

```
#### Ingrese algunos registros: 

```sql
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Juan Perez','22333444','m','Sarmiento 123',500);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Ana Acosta','24555666','f','Colon 134',650);
insert into empleados (nombre, documento, sexo, domicilio, sueldobasico) values ('Bartolome Barrios','27888999','m','Urquiza 479',800);

```
#### Seleccione todos los registros (3 registros)

```sql
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico)
VALUES ('María González','29999888','masculino','Belgrano 567',700);


```
#### Intente ingresar un registro con el valor "masculino" en el campo "sexo".

```sql
INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico)
VALUES ('María González','29999888','masculino','Belgrano 567',700);

```
#### Intente ingresar un valor fuera de rango, en un nuevo registro, para el campo "sueldobasico" Mensaje de error.

```sql

INSERT INTO empleados (nombre, documento, sexo, domicilio, sueldobasico)
VALUES ('Pedro Ramirez','28999777','m','San Martín 890',10000);

```
#### Elimine la tabla
```sql
DROP TABLE empleados;

```


